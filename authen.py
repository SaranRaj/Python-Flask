from flask import Flask,render_template,request, redirect, url_for

app=Flask(__name__)

@app.route("/login", methods=["POST","GET"])

def login():

    return render_template("login.html")
    
    if (request.form["text"] != "sarvini" and request.form["password"] != "sarvini"):
       
        return render_template("error.html")
	
    else:
        return redirect(url_for("/calci"))
    

@app.route("/calci")

def calculator():
    return render_template("calci.html")

@app.route("/cal",methods=["POST","GET"])

def form():
    return render_template("cal.html")

@app.route("/output",methods=["POST"])

def output():
    return str(int(request.form["noa"])+int(request.form["nob"]))

if __name__ == '__main__':
    app.run(port=8005)
