from flask import Flask

app=Flask(__name__)

@app.route("/hello")

def hello():

    return "helloworld"

@app.route("/result")

def mul():
    
    return str(2*4)

if __name__ == "__main__":

    app.run()
