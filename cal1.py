from flask import Flask,render_template

app=Flask(__name__)

@app.route("/calci")

def calculator():

    return render_template("calci.html")

@app.route("/cal",methods=["POST","GET"])

def form():

    return render_template("cal.html")

if __name__=="__main__":

    app.run()
