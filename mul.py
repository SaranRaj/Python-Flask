from flask import Flask

app=Flask(__name__)

def arith_mul():

    a, b = 10, 5

    return str(a*b)

@app.route("/")

def ans():

    return arith_mul()


if __name__ == '__main__':

    app.run()
